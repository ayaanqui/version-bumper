const fs = require("fs");
const filename = "./app.json";
const file = require(filename);

const args = process.argv.slice(2);
let newBuildNumber = "";
if (args.length >= 1) {
  newBuildNumber = args[0];
} else {
  const curBuildVersion = file.version;
  const parsedBuildVersion = curBuildVersion.split(".");
  let major = parsedBuildVersion[0];
  let minor = parsedBuildVersion[1];
  let patch = parsedBuildVersion[2];

  if (patch <= 9) {
    patch = parseInt(patch) + 1;
  } else if (minor <= 9) {
    patch = 0;
    minor = parseInt(minor) + 1;
  } else {
    patch = 0;
    minor = 0;
    major = parseInt(major) + 1;
  }

  newBuildNumber = `${major}.${minor}.${patch}`;
}

file.version = newBuildNumber;
file.ios.buildNumber = newBuildNumber;

fs.writeFile(filename, JSON.stringify(file, null, 2), () => {});

console.log(`🎉🎉🎉 Bumped versions to ${newBuildNumber}`);
